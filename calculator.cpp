#include <iostream>
#include <string>
#include <cmath>
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <limits>
#include <ctype.h>

using namespace std;

// This is a struct that defines a 3D vector for use in calculation.
typedef struct vector {
    long x;
    long y;
    long z;
} Vector;

// This is the token type, and contains all possible tokens in our language.
typedef enum {
  T_PLUS,
  T_MINUS,
  T_MULTIPLY,
  T_CROSS,
  T_OPENPAREN,
  T_CLOSEPAREN,
  T_EQUALS,
  T_M,
  T_PRINT,
  T_NUMBER,
  T_COMMA,
  T_EOF
} token;


// This function will convert a token to a string, for display.
std::string tokenToString(token toConvert) {
  switch (toConvert) {
    case T_PLUS:
        return string("+");
    case T_MINUS:
        return string("-");
    case T_MULTIPLY:
        return string("*");
    case T_CROSS:
        return string("x");
    case T_OPENPAREN:
        return string("(");
    case T_CLOSEPAREN:
        return string(")");
    case T_EQUALS:
        return string("=");
    case T_M:
        return string("m");
    case T_PRINT:
        return string("print");
    case T_NUMBER:
        return string("number");
    case T_COMMA:
        return string(",");
    case T_EOF:
        return string("EOF");
    default:
        return string("Unknown token");
  }
}

// Throw this error when the parser expects a given token from the scanner
// and the next token the scanner finds does not match.
void mismatchError(int line, token expected, token found) {
  std::cerr << "Parse error: expected " << tokenToString(expected) << " found " << tokenToString(found) << " at line " << line << std::endl;
  exit(1);
}

// Throw this error when the parser encounters a token that is not valid
// at the beginning of a specific grammar rule.
void parseError(int line, token found) {
  std::cerr << "Parse error: found invalid token " << tokenToString(found) << " at line " << line << std::endl;
  exit(1);
}

// Throw this error when an invalid character is found in the input, or
// if a bad character is found as part of a print token.
void scanError(int line, char badCharacter) {
  std::cerr << "Scan error: found invalid character " << badCharacter << " at line " << line << std::endl;
  exit(1);
}

// Throw this error when a number is out of bounds, which means greater than signed INT_MAX.
void outOfBoundsError(int line, long number) {
  std::cerr << "Semantic error: number " << number << " out of bounds at line " << line << std::endl;
  exit(1);
}

class Scanner {
	private:
		int line_number;
		int value;

	public:

		Scanner(){
			line_number = 1;
			value = -1;
		}

    token nextToken();
    void eatToken(token);
    int lineNumber();
    int getNumberValue();
    
};

token Scanner::nextToken() {
	char input_token = cin.peek();
	switch(input_token){
		case '+':
			return T_PLUS;
			break;
		case '-':
			return T_MINUS;
			break;
		case '*':
			return T_MULTIPLY;
			break;
		case 'x':
			return T_CROSS;
			break;
    case 'X':
      return T_CROSS;
      break;
		case '(':
			return T_OPENPAREN;
			break;
		case ')':
			return T_CLOSEPAREN;
			break;
		case '=':
			return T_EQUALS;
			break;
		case 'm':
			return T_M;
			break;
    case 'M':
      return T_M;
      break;
		case 'p':{
      bool exit = false;
      cin.get();
      string s = "rint ";
      for(int i = 0; i<5; i++ ){
          if(cin.peek() != s[i]){
              scanError(lineNumber(),cin.peek());
              exit = true;
              break;
          }
          cin.get();
      }
      if(!exit){
          cin.putback(' ');cin.putback('t');cin.putback('n');cin.putback('i');cin.putback('r');cin.putback('p');
          return T_PRINT;
      }
			break;
    }
		case ',':
			return T_COMMA;
			break;
		case EOF:
			return T_EOF;
			break;
		case '\n':{
			line_number++;
			cin.get();
			return nextToken();
			break;
    }
		case ' ':
			cin.get();
			return nextToken();
			break;
		default:
			if(isdigit(input_token)){
					return T_NUMBER;
					break;
			}
			else
				scanError(lineNumber(),input_token);
				break;

	}
  return T_EOF;
}

void Scanner::eatToken(token toConsume) {
	switch (toConsume) {
		case T_PLUS:
			cin.get();
			break;
		case T_MINUS:
			cin.get();
			break;
		case T_MULTIPLY:
			cin.get();
			break;
		case T_CROSS:
			cin.get();
			break;
		case T_OPENPAREN:
			cin.get();
			break;
		case T_CLOSEPAREN:
			cin.get();
			break;
		case T_EQUALS:
			cin.get();
			break;
		case T_M:
			cin.get();
			break;
    case T_PRINT:{  
			string print;
			cin >> print;
			break;
		}
		case T_NUMBER:
			cin >> value;
			break;
		case T_COMMA:
			cin.get();
			break;
		default:
			cin.get();
			break;
	}
	
}

int Scanner::lineNumber() {
  return line_number;
}


int Scanner::getNumberValue() {
  return value;
}


class Parser {
  // You are allowed to private fields to the parser, and this may be
  // necessary to complete your implementation. However, this is not
  // required as part of the project specification.
private:
  Scanner scanner;
  
  // This flag indicates whether we should perform evaluation and throw
  // out-of-bounds and divide-by-zero errors. ONLY evaluate and throw these
  // errors if this flag is set to TRUE.
  bool evaluate;
  
  // You will need to add more methods for handling nonterminal symbols here.
  void Start();
  void Statements();
  void Statement();
  void StatementsPrime();
  
  void Expression_1();
  void Expression_2();
  void Expression_3();
  void Expression_4();

  void ExpressionPlusMinus(Vector vec);
  void ExpressionMult(Vector vec);
  void ExpressionCross(Vector vec);
  void ExpressionOP();


  void checkBounds();
  Vector v;
  Vector m;
    
public:
  void parse();
  
  Parser(bool evaluate) : evaluate(evaluate) {
    v.x = m.x = 0;
    v.y = m.y = 0;
    v.z = m.z = 0;
  }
};

void Parser::parse() {
  Start();
}

void Parser::Start() {
  Statements();
}

void Parser::Statements(){
  Statement();
  StatementsPrime();
}

void Parser::StatementsPrime(){
  token tok = scanner.nextToken();
  if(tok == T_M || tok == T_PRINT){
    Statement();
    StatementsPrime();
  }
  else if (tok == T_EOF){
    return;
  }
  else{
    parseError(scanner.lineNumber(),tok);
  }
}

void Parser::Statement(){
  token tok = scanner.nextToken();
  switch(tok){
    case T_M:{
      scanner.eatToken(tok);
      tok = scanner.nextToken();
      if(tok == T_EQUALS){
        scanner.eatToken(tok);
        checkBounds();
        Expression_1();
        m = v;
      }
      else{
        mismatchError(scanner.lineNumber(),T_EQUALS, tok);
      }
      break;
    }
    case T_PRINT:{
      scanner.eatToken(tok);
      Expression_1();
      if(evaluate){
          cout << "(" << v.x <<", " <<v.y << ", " << v.z <<  ")" << endl;
      }
      break;
    }
    default:
      parseError(scanner.lineNumber(),tok);
      break;

  }
}
void Parser::checkBounds(){
  if(evaluate){
    if(v.x>INT_MAX || v.x < INT_MIN){
      outOfBoundsError(scanner.lineNumber(), v.x);
    }
    else if(v.y>INT_MAX || v.y < INT_MIN){
      outOfBoundsError(scanner.lineNumber(), v.y);
    }
    else if(v.z>INT_MAX || v.z < INT_MIN){
      outOfBoundsError(scanner.lineNumber(), v.z);
    }
  }
}
void Parser::Expression_1(){
  Expression_2();
  ExpressionPlusMinus(v);
}

void Parser::ExpressionPlusMinus(Vector vec){
  token tok = scanner.nextToken();
  switch(tok){
    case T_PLUS:{
      scanner.eatToken(tok);
      Expression_2();
      v.x = v.x + vec.x;
      v.y = v.y + vec.y;
      v.z = v.z + vec.z;
      checkBounds();
      ExpressionPlusMinus(v);
      break;
    }
    case T_MINUS:{
      scanner.eatToken(tok);
      Expression_2();
      v.x = vec.x - v.x;
      v.y = vec.y - v.y;
      v.z = vec.z - v.z ;
      checkBounds();
      ExpressionPlusMinus(v);
      break;
    }
    case T_M:
      break;
    case T_EOF:
      break;
    case T_PRINT:
      break;
    case T_CLOSEPAREN:
      break;  
    default:{
      parseError(scanner.lineNumber(),tok);
      break;
    }
  }
}



void Parser::Expression_2(){
  Expression_3();
  ExpressionCross(v);
}

void Parser::ExpressionCross(Vector vec){                                                                                                                                                                                                      
  token tok = scanner.nextToken();
  switch(tok){
    case T_CROSS:{
      scanner.eatToken(tok);
      Expression_3();
      ExpressionCross(v);
      long x = -(v.y*vec.z - v.z*vec.y);
      long y = -(v.z*vec.x - v.x*vec.z);
      long z = -(v.x*vec.y - v.y*vec.x); 
      v.x = x;
      v.y = y;
      v.z = z;
      checkBounds();
      break;
    }
    case T_PLUS:
      break;
    case T_MINUS:
      break;
    case T_M:
      break;
    case T_PRINT:
      break;
    case T_EOF:
      break;
    case T_CLOSEPAREN:
      break;
    default:{
      parseError(scanner.lineNumber(),tok);
      break;
    }
  }
}

void Parser::Expression_3(){  
  Expression_4();
  ExpressionMult(v);
}

void Parser::ExpressionMult(Vector vec){
  token tok = scanner.nextToken();
  switch(tok){
    case T_MULTIPLY:{
      scanner.eatToken(tok);
      Expression_4();
      v.x = v.x * vec.x;
      v.y = v.y * vec.y;
      v.z = v.z * vec.z;
      checkBounds();
      ExpressionMult(v);
      break;
    }
    case T_CROSS:
      break;
    case T_PLUS:
      break;
    case T_MINUS:
      break;
    case T_M:
      break;
    case T_PRINT:
      break;
    case T_EOF:
      break;
    case T_CLOSEPAREN:
      break;
    default:{ 
      parseError(scanner.lineNumber(),tok);
      break;
    }
  }
}

void Parser::Expression_4(){
  token tok = scanner.nextToken();
  switch(tok){
    case T_M:{
      scanner.eatToken(T_M);
      v = m;
      break;  
    }
    case T_OPENPAREN:{
        scanner.eatToken(T_OPENPAREN);
        ExpressionOP();
        tok = scanner.nextToken();
        if(tok == T_CLOSEPAREN){
          scanner.eatToken(tok);
          break;
        }
        else{
          mismatchError(scanner.lineNumber(), T_CLOSEPAREN, tok);
        }
    }
    default:
      parseError(scanner.lineNumber(), tok);
      break;
  }
}

void Parser::ExpressionOP(){ 
  token tok = scanner.nextToken();
  switch(tok){
    case T_NUMBER:{
      scanner.eatToken(tok); 
      v.x = scanner.getNumberValue();
      tok = scanner.nextToken();
      if(tok==T_COMMA){
        scanner.eatToken(T_COMMA); 
      }
      else{
        mismatchError(scanner.lineNumber(),T_COMMA,tok);
        break;
      }
      tok = scanner.nextToken();
      if(tok==T_NUMBER){
        scanner.eatToken(T_NUMBER); 
        v.y = scanner.getNumberValue();
      }
      else{
        mismatchError(scanner.lineNumber(),T_NUMBER,tok);
        break;
      }
      tok = scanner.nextToken();
      if(tok==T_COMMA){           
        scanner.eatToken(T_COMMA);
      }
      else{
        mismatchError(scanner.lineNumber(),T_COMMA,tok);
        break;
      }
      tok = scanner.nextToken();
      if(tok==T_NUMBER){
        scanner.eatToken(T_NUMBER); 
        v.z = scanner.getNumberValue();
      }
      else{
        mismatchError(scanner.lineNumber(),T_NUMBER,tok);
        break;
      }
      break;
    }
    default:{
      Expression_1();
      break;
    }

  }
}

int main(int argc, char* argv[]) {
  if (argc == 2 && (strcmp(argv[1], "-s") == 0)) {
    Scanner scanner;
    while (scanner.nextToken() != T_EOF) {
        std::cout << tokenToString(scanner.nextToken()) << " ";
        scanner.eatToken(scanner.nextToken());
    }
    std::cout<<std::endl;
  } else if (argc == 2 && (strcmp(argv[1], "-e") == 0)) {
    Parser parser(true);
    parser.parse();
  } else {
    Parser parser(false);
    parser.parse();
  }
  return 0;
}
